//
//  SearchSachen.swift
//  TabellenTest
//
//  Created by Tony Borchert on 25.05.20.
//  Copyright © 2020 Tony Borchert. All rights reserved.
//

import Foundation


    

open class QSObject<T>:Codable,Hashable where T:QSProtocol,T:Codable {
    
    public static func == (lhs: QSObject<T>, rhs: QSObject<T>) -> Bool {
        lhs === rhs
    }
    
    var importance = 0.0
    
    var realImportance:Double {
        self.importance / Double(self.keys.count)
    }
    
    
    let object:T
    
    init(object:T) {
        self.object = object
        
    }
    var keys:[String] {
        self.object.searchKeys
    }
    

    public func hash(into hasher: inout Hasher) {
        hasher.combine(ObjectIdentifier(self))
        
    }
}

open class QSHandler<T>:Codable where T:QSProtocol,T:Codable {
    
    private var dictionary:[String:[QSObject<T>]]
    
    private var objects:[QSObject<T>]
    
    
    public var searchObjects:[QSObject<T>] {
        objects
    }
    
    public var tolerance:Double = 0.25
    
    
    public init(_ objects:[QSObject<T>]) {
        self.objects = objects
        dictionary = [:]
        for object in objects {
            addToDictionary(object: object)
        }
        
    }
    
    public init(_ objects:[T]) {
        self.objects = [QSObject<T>]()
        dictionary = [:]
        for object in objects {
            let objectToAppend = QSObject(object: object)
            addToDictionary(object: objectToAppend)
            self.objects.append(objectToAppend)
        }
    }
    
    private func addToDictionary(object:QSObject<T>) {
        for key in object.keys {
            if key != key.lowercased() {
                if dictionary[key.lowercased()] != nil {
                    dictionary[key.lowercased()]?.append(object)
                } else {
                    dictionary[key.lowercased()] = [object]
                }
            }
            if dictionary[key] != nil {
                dictionary[key]?.append(object)
            } else {
                dictionary[key] = [object]
            }
        }
    }
    
    public func appendObject(_ element: T) {
        let objectToAppend = QSObject(object: element)
        self.objects.append(objectToAppend)
        addToDictionary(object: objectToAppend)
    }
    
    public func appendObject(_ element: QSObject<T>) {
        self.objects.append(element)
        addToDictionary(object: element)
    }
    
    public func search(for searchString:String) -> [T] {
        var searchResults:Set<QSObject<T>> = []
            
        var searchKeywords:Array<String> = [""]
        
        for character in searchString {
            if character == " " {
                searchKeywords.append("")
            } else {
                searchKeywords[searchKeywords.count - 1].append(character)
            }
        }
        searchKeywords.removeAll { (str) -> Bool in
            str == " "
        }
        for searchKeyword in searchKeywords {
            for key in dictionary.keys {
                print(key)
                let searchImportance = searching(searchString: searchKeyword, wordToSearch: key)
                print(searchImportance)
                
                if let objectsToModify = dictionary[key]  {
                    for objectToModify in objectsToModify {
                        objectToModify.importance += searchImportance
                        if objectToModify.realImportance > tolerance {
                            searchResults.insert(objectToModify)
                        }
                        
                    }
                }

                
            }
        }
        
        var finalResults:[T] = []
        
        for result in (searchResults.sorted(by: { (first, second) -> Bool in
            first.realImportance > second.realImportance
            
        })) {
            print("____-",result.importance,result.realImportance,result.object)
           finalResults.append(result.object)
        }
        
        for object in self.objects {
            object.importance = 0
        }
        return finalResults
    }
    
    
    
    private func searching(searchString:String, wordToSearch:String) -> Double {
        let search = Array(searchString.lowercased())
        let _object = Array(wordToSearch.lowercased())
        var highestRanking = 0.0
        
        var object = _object
        
        
        
        _for:for i in 0..._object.count - 1 {
            if i != 0 {
                object.removeFirst()
            }
            
            var ultraRank = 0.0
            var ranking = 0.0
            var secondIndex = 0
            var characters = ""
            
            
            first:for character in search {
                
                second:for index in secondIndex...object.count - 1 {
                    
                    
                    
                    if character == object[index] {
                        characters.append(character)
                        
                        
                        
                        if ultraRank == 0 {
                            ultraRank = 1
                        } else {
                            ultraRank += ultraRank
                        }
                        ranking += ultraRank
                        
                        
                        if !(index + 1 > object.count - 1) {
                           secondIndex = index + 1
                        } else {
                            break first
                        }
                        
                        
                        continue first
                        
                    } else {
                        ultraRank = 0
                    }
                }
                //ranking *= 0.8
                
            }
            
            if search.count == 1 && i == 0 && search[0] == _object[0] {
                ranking *= 2.5
            }
            
            if searchString.contains(wordToSearch) {
                
                ranking *= 2
            
            } else if searchString.lowercased().contains(wordToSearch.lowercased()) {
                ranking *= 1.5
            }
            ranking -= Double(i) * 0.1
            ranking -= Double(object.count) * 0.1
            print(object,ranking)
            
            if ranking > highestRanking {
                highestRanking = ranking
            }
            print(characters,"characters")
            
        }
        
        return highestRanking
    }
}


//extension String:QSSimpleProtocol {
//    public var dumpKeys: String { self }
//}

public protocol QSProtocol {
    var searchKeys:[String] { get }
    
}



public protocol QSSimpleProtocol:QSProtocol {
    var dumpKeys:String { get }
    var alternativeKeys:[String] { get }
}

public extension QSSimpleProtocol {
    var searchKeys: [String] {
        get {
            var returnValue = [""]
            for character in dumpKeys {
                if character == " " {
                    returnValue.append(String())
                } else {
                    returnValue[returnValue.count - 1].append(character)
                }
            }
            returnValue.removeAll { (str) -> Bool in
                str == ""
            }
            return returnValue + alternativeKeys
        }
    }
    var alternativeKeys:[String] { [] }
    var dumpKeys:String {
        String()
    }
    
}

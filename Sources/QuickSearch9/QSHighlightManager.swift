//
//  EasyMutableString with Betterseach 4.1.swift
//  TabellenTest
//
//  Created by Tony Borchert on 31.05.20.
//  Copyright © 2020 Tony Borchert. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
final public class QSHighlightManager {
    
    private init() { }
    
    private static func searching(searchString:String, wordToSearch:String) -> [(Character,Bool)] {
        
        let _object = Array(wordToSearch.lowercased())
        let search = Array(searchString.lowercased())
        if search.count == 1 {
            let character = search.first!
            if let index:Int = _object.firstIndex(of: character) {
                var string = EasyMutableString(string: wordToSearch).string
                string[index].1 = true
               return string
                
            } else {
                return EasyMutableString(string: wordToSearch).string
            }
            
            
        } else if search.count == 0 {
            return EasyMutableString(string: wordToSearch).string
        }
        
        var highestRanking = 0.0
            var wordInformation:[(Character,Bool)] = []
            
            var object = _object
            _for:for i in 0..._object.count - 1 {
                if i != 0 {
                    object.removeFirst()
                }
                var zwischenWordInformation:[(Character,Bool)] = []
                var counter = 0
                while counter < _object.count {
                    zwischenWordInformation.append((_object[counter],false))
                    counter += 1
                }
                
                
                var ultraRank = 0.0
                var ranking = 0.0
                var secondIndex = 0
                var characters = ""
                
                
                first:for character in search {
                    //print(search,"search")
                    var wrongHasBeenDocumented = false
                    
                    second:for index in secondIndex...object.count - 1 {

                        //print(character,"everyCharacter")
                        if character == object[index] {

                            //print(character,"character")
                            characters.append(character)
                            if wrongHasBeenDocumented {
                                //print("before")
                                //print(index)//
                                //print(zwischenWordInformation[index],(character,true),"wrongHasBeenDocumented")
                                zwischenWordInformation[index + i] = (character,true)
                                //print("after")
                            } else {
                                //print(zwischenWordInformation[index],(character,true),"wrongHasNotBeenDocumented")
                                //print(zwischenWordInformation, index,character)
                                zwischenWordInformation[index + i] = (character,true)

                                wrongHasBeenDocumented = true
                            }

        //                    if ultraRank == 0 {
        //                        if searchString.count < 2  {
        //                            ranking += 3
        //                        } else {
        //                            ranking += 1
        //                        }
        //                    } else {
        //                        ultraRank += 2
        //
        //                        ranking += (ultraRank * ultraRank)
        //                    }
                            if ultraRank == 0 {
                                ultraRank = 1
                            } else {
                                ultraRank += ultraRank
                            }
                            ranking += ultraRank


                            if !(index + 1 > object.count - 1) {
                               secondIndex = index + 1
                            } else {
                                break first
                            }


                            continue first

                        } else {
                            if !wrongHasBeenDocumented {
                                //print(character,object[index], false)
                                //zwischenWordInformation.append((object[index],false))
                                ultraRank = 0
                                wrongHasBeenDocumented = true
                            }

                        }
                    }
                    //ranking *= 0.8
                    
                }
                
                if searchString.contains(wordToSearch) {
                    
                    ranking *= 2
                
                } else if searchString.lowercased().contains(wordToSearch.lowercased()) {
                    ranking *= 1.5
                }
                ranking -= Double(i) * 0.1
                ranking -= Double(object.count) * 0.1
                //print(object,ranking)
                
                if ranking > highestRanking {
                    highestRanking = ranking
                    wordInformation = zwischenWordInformation
                }
                
                
            }
            
            
            
            var ultraWordInformation = [(Character,Bool)]()
            
            for character in wordToSearch {
                ultraWordInformation.append((character,false))
            }
            //wordInformation.append(("j", false))
        if wordInformation.count == 0 {
            return []
        }
            for index in 0...wordInformation.count - 1 {
                ultraWordInformation[index].1 = wordInformation[index].1
            }
        
        return wordInformation
        
    }
    
    static func getHighlightInformation(searchRequest:String,wordToSearch:String) -> [(Character,Bool)] {
        var wordsToSearch:[[(Character,Bool)]] = [[]]
        
        for character in wordToSearch {
            if character == " " {
                wordsToSearch.append([])
            } else {
                wordsToSearch[wordsToSearch.count - 1].append((character,false))
            }
        }
        wordsToSearch.removeAll { (value) -> Bool in
            value.count == 0
        }
        var searches:[String] = [String()]
        for character in searchRequest {
            if character == " " {
                searches.append(String())
            } else {
                searches[searches.count - 1].append(character)
            }
        }
        searches.removeAll { (string) -> Bool in
            string == ""
        }
        
        for index in 0..<wordsToSearch.count {
            for search in searches {
                let searchResults = searching(searchString: search, wordToSearch: wordsToSearch[index].inString)
                if searchResults.count != 0 {
                    for i in 0...searchResults.count - 1 {
                        if searchResults[i].1 {
                            wordsToSearch[index][i].1 = true
                            
                        }
                        //wordsToSearch[index][i].1 = searchResults[i]
                    }

                }
            }
            //wordsToSearch[index] = searching(searchString: searchRequest, wordToSearch: giveString(wordsToSearch[index]))
        }
        var newString:[(Character,Bool)]  = Array()
        for string in wordsToSearch {
            newString += string + [(" ",false)]
        }
        if newString.count != 0 {
            newString.removeLast()
        }
        
        print(newString,newString.inString.count,newString.inString,"newString (important)")
        
        return newString
        
    }
    
    static func highlightLabel(searchRequest:String,highlightLabel:QSHighlightLabel) {
        var wordsToSearch:[[(Character,Bool)]] = [[]]
        guard let wordToSearch = highlightLabel.text else {
            return
        }
        for character in wordToSearch {
            if character == " " {
                wordsToSearch.append([])
            } else {
                wordsToSearch[wordsToSearch.count - 1].append((character,false))
            }
        }
        wordsToSearch.removeAll { (value) -> Bool in
            value.count == 0
        }
        var searches:[String] = [String()]
        for character in searchRequest {
            if character == " " {
                searches.append(String())
            } else {
                searches[searches.count - 1].append(character)
            }
        }
        searches.removeAll { (string) -> Bool in
            string == ""
        }
        
        for index in 0..<wordsToSearch.count {
            for search in searches {
                let searchResults = searching(searchString: search, wordToSearch: wordsToSearch[index].inString)
                if searchResults.count != 0 {
                    for i in 0...searchResults.count - 1 {
                        if searchResults[i].1 {
                            wordsToSearch[index][i].1 = true
                            
                        }
                        //wordsToSearch[index][i].1 = searchResults[i]
                    }

                }
            }
            //wordsToSearch[index] = searching(searchString: searchRequest, wordToSearch: giveString(wordsToSearch[index]))
        }
        var newString:[(Character,Bool)]  = Array()
        for string in wordsToSearch {
            newString += string + [(" ",false)]
        }
        if newString.count != 0 {
            newString.removeLast()
        }
        
        //print("bruh",highlightLabel.text,newString.inString,"newString (important)")
        
        highlightLabel.characters = newString
        
        
    }
    
}




public extension UITextField {
    func convertRange(_ range: NSRange) -> UITextRange? {
      let beginning = beginningOfDocument
      if let start = position(from: beginning, offset: range.location), let end = position(from: start, offset: range.length) {
        let resultRange = textRange(from: start, to: end)
        return resultRange
      } else {
        return nil
      }
    }

    func frame(ofRange range: NSRange) -> [CGRect]? {
      if let textRange = convertRange(range) {
        let rects = selectionRects(for: textRange)
        return rects.map { $0.rect }
      } else {
        return nil
      }
    }
}




@available(iOS 10.0, *)
public class QSHighlightLabel:UITextField {
    
    public struct HighlightColors {
        var highlightColors:UIColor
        var notHighlightColors:UIColor
    }
    
    private var cachedFrames: [CGRect] = []

    private var backgrounds: [UIView] = []

    public var characters:[(Character,Bool)]? = nil {
        didSet {
            if let characters = characters {
                self.text = characters.inString
            }
            
            
        }
    }
    
    public override var canBecomeFocused: Bool {
        false
    }
    
    public override var canBecomeFirstResponder: Bool {
        false
    }
    
    public var shouldHighlight:Bool = true {
        didSet {
            self.textUpdated()
        }
    }
    
    public var highlightColors:HighlightColors = HighlightColors(highlightColors: UIColor(displayP3Red: 0.981, green: 0.898, blue: 0.365, alpha: 1), notHighlightColors: UIColor.systemBlue)
    
    public override init(frame: CGRect) {
        super.init(frame:frame)
        configureView()
    }

      public required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureView()
        self.borderStyle = .none
        self.isEnabled = false
        
      }

      public override func layoutSubviews() {
        super.layoutSubviews()

        // Redraw highlighted parts if frame is changed
        textUpdated()
      }

    deinit {
        NotificationCenter.default.removeObserver(self)
      }

      @objc private func textUpdated() {
        // You can provide whatever ranges needed to be highlighted
        
        guard let characters = characters, let text = text else {
            return
        }
        let ranges = characters.inRanges
        
        
        if shouldHighlight {
            let frames = ranges.compactMap { frame(ofRange: $0) }.reduce([], +)
            
            let attributedString = NSMutableAttributedString(string: text)
            
            for range in ranges {
                attributedString.addAttribute(.foregroundColor, value: UIColor.black, range: range)
            }
            self.attributedText = attributedString
            
            if cachedFrames != frames {
              cachedFrames = frames

              backgrounds.forEach { $0.removeFromSuperview() }
              backgrounds = cachedFrames.map { frame in
                let background = UIView()
                background.backgroundColor = highlightColors.highlightColors
                
                var frame2 = frame
                frame2.size.height *= 0.9
                background.frame = frame2
                background.layer.cornerRadius = 3
                insertSubview(background, at: 0)
                return background
              }
            }
        } else {
            
            var easyMutableAttributedString = EasyMutableString(string: characters)
            easyMutableAttributedString.other = [NSAttributedString.Key.foregroundColor : self.highlightColors.notHighlightColors]
            easyMutableAttributedString.regular = [:]
            self.attributedText = easyMutableAttributedString.mutableAttributedString
            
        }
        
        
        
      }

      /// General setup
      private func configureView() {
        NotificationCenter.default.addObserver(self, selector: #selector(textUpdated), name: UITextView.textDidChangeNotification, object: self)
      }

    
}



public extension UITextView {
  func convertRange(_ range: NSRange) -> UITextRange? {
    let beginning = beginningOfDocument
    if let start = position(from: beginning, offset: range.location), let end = position(from: start, offset: range.length) {
      let resultRange = textRange(from: start, to: end)
      return resultRange
    } else {
      return nil
    }
  }

  func frame(ofRange range: NSRange) -> [CGRect]? {
        if let textRange = convertRange(range) {
            let rects = selectionRects(for: textRange)
            return rects.map { $0.rect }
        } else {
            return nil
        }
    }
        
        
}

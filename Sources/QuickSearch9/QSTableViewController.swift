//
//  ViewController.swift
//  TabellenTest (mit searchbar, splitcontroller, tabellencontroller)
//
//  Created by Tony Borchert on 04.08.20.
//  Copyright © 2020 Tony Borchert. All rights reserved.
//

import UIKit

///Best QSTableViewController class (and the only one)

@available(iOS 11.0, *)
open class QSTableViewController<T>: UITableViewController,UISearchBarDelegate,QSSourceObjectChangeDelegate,UIContextMenuInteractionDelegate where T:QSTableViewCellProtocol,T:AnyObject {
    
    
    
    
    
    
    //MARK: - variablen
    
    public var source = [T]()
    
    public var searchSource = [T]()
    
    
    public var searchHandler:QSHandler<T>!
    
    private var selectionColor:UIColor = UIColor.dynamicColor(light: UIColor(red: 209/255.0, green: 209/255.0, blue: 214/255.0, alpha: 1), dark: UIColor(red: 58/255.0, green: 58/255.0, blue: 58/255.0, alpha: 1))
    
    
    
    //MARK: - Nutzungs Variablen
    
    open var isSearching = false {
           didSet {
            if searchEnabled {
                if isSearching == true {
                    
                    if isEditingTableView {
                        isEditingTableView = false
                        self.navigationItem.rightBarButtonItem?.style = .plain
                        self.navigationItem.rightBarButtonItem?.title = "edit"
                    }
                    if self.navigationItem.searchController?.searchBar.isFirstResponder == false {
                        navigationItem.searchController?.searchBar.becomeFirstResponder()
                    }
                } else {
                    
                        
                    
                    
                    
                    self.searchSource = []
                    
                    if self.navigationItem.searchController?.isActive == true {
                        self.navigationItem.searchController?.isActive = false
                    }
                    
                    
                    self.tableView.reloadData()
                    
                    
                }
            } else if isSearching {
                
                isSearching = false
            }
               
           }
       }
    
    open var isEditingTableView = false {
        didSet {
            if isEditingTableView {
                if !editingEnabled {
                    isEditingTableView = false
                } else {
                    self.tableView.isEditing = true
                }
                
            } else {
                self.tableView.isEditing = false
            }
        }
    }
    
    open var searchEnabled:Bool = true {
           didSet {
               if !searchEnabled {
                   self.navigationItem.searchController = nil
               } else {
                   let searchController = UISearchController(searchResultsController: nil)
                   searchController.searchBar.delegate = self
                          
                   self.navigationItem.searchController = searchController
                   self.navigationItem.hidesSearchBarWhenScrolling = false
                if isSearching {
                    isSearching = false
                }
                
               }
           }
       }
    
    open var editingEnabled = true {
        didSet {
            if !editingEnabled {
                isEditingTableView = false
                self.navigationItem.rightBarButtonItem = nil
            } else {
                if isEditingTableView {
                    self.navigationItem.rightBarButtonItem?.title = "Done"
                    self.navigationItem.rightBarButtonItem?.style = .done
                    
                } else {
                    self.navigationItem.rightBarButtonItem?.title = "Done"
                    self.navigationItem.rightBarButtonItem?.style = .plain
                }
                
            }
        }
    }
    
    
    
    //MARK: - viewDidLoad()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        let searchController = UISearchController(searchResultsController: nil)
        
        searchController.searchBar.delegate = self
        searchController.searchBar.returnKeyType = .done
        searchController.searchBar.autocapitalizationType = .none
        searchController.obscuresBackgroundDuringPresentation = false
        
        
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        
        self.clearsSelectionOnViewWillAppear = true
        
        ändern(searchBar: searchController.searchBar, navigationItem: self.navigationItem, navigationController: self.navigationController!)
        
        source = getSource()
        
        update()
        
        if source.count == 0 {
            return
        }
        
        var indexPath:IndexPath?
        var counter = 0
        for object in source {
            if object.isSelected {
                indexPath = IndexPath(row: 0, section: counter)
            }
            counter += 1
        }
        
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let indexPath = indexPath {
                self.select(selectRow: indexPath)
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            } else {
                let indexPath = IndexPath(item: 0, section: 0)
                self.select(selectRow: indexPath)
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)

            }
        }
        
        
    }
    
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.navigationBar.sizeToFit()
        }
        
        self.isSearching = false
        self.isEditing = false
        self.navigationItem.searchController?.isActive = false
        self.updateViewConstraints()
        
        guard source.count > 0 else {
            return
        }
        
        
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.isSearching = false
            self.isEditing = false
        }
       
    }
    
    
    
    
    //MARK: - User funktionen
    
    open func ändern(searchBar:UISearchBar,navigationItem:UINavigationItem,navigationController:UINavigationController) {
        
    }
    
    
    open func getSource() -> [T] {
        return []
    }
    
    open func getSearchHandler(source: [T]) -> QSHandler<T>? {
        nil
    }
    
    open func getCell(indexPath:IndexPath,sourceObject:T,searchString:String? = nil) -> QSTableViewCell<T> {
        QSTableViewCell<T>()
    }
    
    open func getDetailViewController(indexPath:IndexPath,sourceObject:T) -> QSViewController? {
        return nil
    }
    
    open func gotTapped(indexPath:IndexPath,sourceObject:T) {
        
    }
    
    open func saveSource(source:[T]) {
        
    }
    
    @available(iOS 13.0, *)
    open func getActionsForContextMenu(indexPath:IndexPath,source:T) -> [UIAction] {
        return []
    }
    
    
    //MARK: - Nutzungs Funktionen
    
    open func update() {
        saveSource(source: self.source)
        searchHandler = getSearchHandler(source: source)
        
        isSearching = false
        
    }
    
   open func resetSelection() {
    
    if source.count == 0 {
        return
    }
        
        for index in 0...source.count - 1 {
            source[index].isSelected = false
            
            
        }
        
        saveSource(source: source)
        
        
    }
    
    
    open func addSourceObject(position:QSTableViewPosition,object:T) {
        resetSelection()
        let indexPath:IndexPath
        
        
        
        switch position {
            case .first:
                indexPath = IndexPath(row: 0, section: 0)
            
            
            case .specific(let index):
                indexPath = index
            
        }
        
        
        self.source.insert(object, at: indexPath[1])
        self.tableView.insertRows(at: [indexPath], with: .automatic)
        
        select(selectRow: indexPath)
        
        
    }
    
    //MARK: - barButtonItem
    
    @IBAction func editBarButton_tapped(_ sender: UIBarButtonItem) {
        guard !isSearching else {
            return
        }
        if sender.title == "Edit" {
            self.navigationItem.rightBarButtonItem?.title = "Done"
            self.navigationItem.rightBarButtonItem?.style = .done
        } else {
            self.navigationItem.rightBarButtonItem?.title = "Edit"
            self.navigationItem.rightBarButtonItem?.style = .plain
        }
        isEditingTableView = sender.title == "Done"
    }
    
    
    //MARK: - QSDetailViewControllerDelegate
    
    
    open func saveAndUpdate() {
        
        saveSource(source: self.source)
        searchHandler = getSearchHandler(source: source)
        
        isSearching = false
        tableView.reloadData()
        
    }
    
    public func deleteSelf(indexPath:IndexPath) {
        
        
        let sourceObject:T
        if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0  {
            sourceObject = searchSource[indexPath[1]]
        } else {
            sourceObject = source[indexPath[1]]
        }
        
        source.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        saveSource(source: source)
        if sourceObject.isSelected {
            if source.count > 1 {
                select(selectRow: IndexPath(row: 0, section: 0))
            }
        }
    }
    
    //MARK: - UISearchBarDelegate
    
    open var lastIndex:IndexPath?
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard isSearching else {
            return
        }
        
        if searchText.count != 0 {
            searchSource = searchHandler.search(for: searchText)
            
            tableView.reloadData()
            guard let firstIndex = searchSource.firstIndex(where: { (tableViewCell) -> Bool in
                tableViewCell.isSelected
            }) else {
                return
            }
            
            let indexPath:IndexPath = IndexPath(indexes: [0,firstIndex])
            
            
            
            for index in 0...searchSource.count - 1 {
                
                guard let cell = self.tableView.cellForRow(at: IndexPath(indexes: [0,index])) as? QSTableViewCell<T> else {
                    return
                }
                
                cell.deselect()
                
                searchSource[index].isSelected = false
                
                
            }
            guard let cell = self.tableView.cellForRow(at: indexPath) as? QSTableViewCell<T> else {
                return
            }
            if UIDevice.current.userInterfaceIdiom == .pad {
                cell.select(selectionColor: self.selectionColor)
                searchSource[indexPath[1]].isSelected = true
            }
            
            
            
            
        } else {
            
            tableView.reloadData()
        }
        

    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        
        searchHandler = getSearchHandler(source: source)
        
        isSearching = true
        
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        isSearching = false
        
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        isSearching = false
        
        
    }
    
    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        !(source.count == 0)
    }
    //MARK: - UIContextMenuInteractionDelegate
    
    @available(iOS 13.0, *)
    public func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        
        guard let indexPath = (interaction.view as? QSTableViewCell<T>)?.indexpath else {
            return nil
        }
        
        var sourceObject:T
        if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0  {
            
             sourceObject = searchSource[indexPath[1]]
        } else {
            
             sourceObject = source[indexPath[1]]
        }
        
        
        return UIContextMenuConfiguration(identifier: nil, previewProvider: { () -> UIViewController? in
           
            self.getDetailViewController(indexPath: indexPath, sourceObject: sourceObject)
            
            
        }) { (_) -> UIMenu? in
            let edit:UIAction
            
            if self.isEditingTableView {
                edit = UIAction(title: "Edit", image: UIImage(systemName: "square.and.pencil"), attributes: .disabled) { _ in }
            } else {
                edit = UIAction(title: "Edit", image: UIImage(systemName: "square.and.pencil")) { _ in
                    self.isEditingTableView = true
                    self.navigationItem.rightBarButtonItem?.title = "Done"
                    self.navigationItem.rightBarButtonItem?.style = .done
                    
                }
            }
            
            
            
            let delete = UIAction(title: "Delete", image: UIImage(systemName: "trash"), attributes: .destructive) { (_) in
                
                if self.isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0  {
                            
                sourceObject = self.searchSource[indexPath[1]]
                        } else {
                            
                sourceObject = self.source[indexPath[1]]
                        }
                        if sourceObject.isSelected && UIDevice.current.userInterfaceIdiom == .pad {
                            if self.source.count > 1 {
                                
                                
                                self.select(selectRow: IndexPath(row: 0, section: 0))
                            }
                        }
                
                            self.source.remove(at: indexPath.row)
                
                            
                            self.tableView.reloadData()
                            
            
            }
            
            
            
            return UIMenu(title: "", image: nil, identifier: nil, options: .displayInline, children: [edit,delete] + self.getActionsForContextMenu(indexPath: indexPath, source: sourceObject))
        }
        
        
        
        
    }
    
    
    //MARK: - UITableViewDelegate
    
    open override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != self.tableView {
            return
        }
        select(selectRow: indexPath)
        
    }
    
    
    
    func select(selectRow indexPath: IndexPath) {
        tableView.reloadData()
        var sourceObject:T
        if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0  {
            
             sourceObject = searchSource[indexPath[1]]
        } else {
            
             sourceObject = source[indexPath[1]]
        }
        
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0 {
                for index in 0...searchSource.count - 1 {
                    
                    
                    searchSource[index].isSelected = false
                }
            } else {
                for index in 0...source.count - 1 {
                    
                    
                    source[index].isSelected = false
                }
            }
            if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0 {
                for index in 0...searchSource.count - 1 {
                    
                    
                    searchSource[index].isSelected = false
                }
            } else {
                for index in 0...source.count - 1 {
                    
                    
                    source[index].isSelected = false
                }
            }
            
            
            
        }
        sourceObject.isSelected = true
        
        gotTapped(indexPath: indexPath, sourceObject: sourceObject)
        
        
        guard var detailViewController = getDetailViewController(indexPath: indexPath, sourceObject: sourceObject) else { tableView.reloadData();return }
        
        detailViewController.sourceDelegate = self
        
        self.splitViewController?.showDetailViewController(detailViewController, sender: nil)
        
        
        tableView.reloadData()
        
        
    }
    
    
    open override func tableView(_ tableView: UITableView,
                                 willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        nil
    }
    
    
    
    
    
    //MARK: - UITableViewDataSource
    
    
    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0 {
            return searchSource.count
        }
        return source.count
    }
    
   
    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sourceObject:T
        let cell:QSTableViewCell<T>
        
        if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0  {
            
            let searchString = self.navigationItem.searchController?.searchBar.text
            sourceObject = searchSource[indexPath[1]]
            cell = getCell(indexPath: indexPath, sourceObject: sourceObject, searchString: searchString)
            cell.object = sourceObject
            cell.indexpath = indexPath
            if !cell.object.isSelected && UIDevice.current.userInterfaceIdiom == .pad {
                cell.deselect()
            } else if UIDevice.current.userInterfaceIdiom == .pad {
                cell.select(selectionColor: self.selectionColor)
            }
        } else {
            
            sourceObject = source[indexPath[1]]
            cell = getCell(indexPath: indexPath, sourceObject: sourceObject, searchString: nil)
            cell.object = sourceObject
            cell.indexpath = indexPath
            if !cell.object.isSelected && UIDevice.current.userInterfaceIdiom == .pad {
                cell.deselect()
            } else if UIDevice.current.userInterfaceIdiom == .pad {
                cell.select(selectionColor: self.selectionColor)
            }
            
            
        }
        
        if #available(iOS 13.0, *) {
            
            let interaction = UIContextMenuInteraction(delegate: self)
            cell.addInteraction(interaction)
        }
        cell.sourceDelegate = self
        return cell
    }
    
        
    
    open override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let movedObject = source[sourceIndexPath.row]
        source.remove(at: sourceIndexPath.row)
        source.insert(movedObject, at: destinationIndexPath.row)
        
        saveSource(source: source)
        tableView.reloadData()
    }
    
    open override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let sourceObject:T
        if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0  {
            sourceObject = searchSource[indexPath[1]]
        } else {
            sourceObject = source[indexPath[1]]
        }
        
        source.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        saveSource(source: source)
        if sourceObject.isSelected {
            if source.count > 1 {
                select(selectRow: IndexPath(row: 0, section: 0))
            }
        }
    }
    
    
    open override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if isSearching && self.navigationItem.searchController?.searchBar.text?.count ?? 1 > 0  {
            return UITableViewCell.EditingStyle.none
        }
        return UITableViewCell.EditingStyle.delete
    }
    
    
}



// MARK: - QSTableViewCell


open class QSTableViewCell<T>:UITableViewCell where T:QSTableViewCellProtocol {
    
    public var sourceDelegate:QSSourceObjectChangeDelegate?
    
    public var object:T!
    
    public var indexpath:IndexPath!
    
    open func select(selectionColor:UIColor) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = selectionColor
        self.backgroundView = backgroundView
        self.selectedBackgroundView = nil
        
    }
    
    open func deselect() {
        
        
        backgroundView = nil
        self.object.isSelected = false
    }
    
    public override var selectionStyle: UITableViewCell.SelectionStyle {
        get {
            UITableViewCell.SelectionStyle.none
        }
        set(i){ }
        
    }
    
   
}

//MARK: - QSViewController

public typealias QSViewController = UIViewController & QSSourceObjectChangeDelegateProtocol


//MARK: - QSSourceObjectChangeDelegateProtocol

public protocol QSSourceObjectChangeDelegateProtocol {
    var sourceDelegate:QSSourceObjectChangeDelegate? { get set }
}


//MARK: - QSSourceObjectChangeDelegate

public protocol QSSourceObjectChangeDelegate {
    func deleteSelf(indexPath:IndexPath)
    
    func saveAndUpdate()
    
}

//MARK: - QSTableViewPosition

public enum QSTableViewPosition {
    case first
    
    case specific(IndexPath)
}


//MARK: - QSTableViewCellProtocol

public protocol QSTableViewCellProtocol:QSProtocol,Codable {
    var dumpKeys:String { get }
    var alternativeKeys:[String] { get }
    var isSelected:Bool { get set }
}

public extension QSTableViewCellProtocol {
    var searchKeys: [String] {
        get {
            var returnValue = [""]
            for character in dumpKeys {
                if character == " " {
                    returnValue.append(String())
                } else {
                    returnValue[returnValue.count - 1].append(character)
                }
            }
            returnValue.removeAll { (str) -> Bool in
                str == ""
            }
            return returnValue + alternativeKeys
        }
    }
    var alternativeKeys:[String] { [] }
    var dumpKeys:String {
        String()
    }
    
}


//MARK: - extension UIColor

public extension UIColor {
    static func dynamicColor(light: UIColor, dark: UIColor) -> UIColor {
        guard #available(iOS 13.0, *) else { return light }
        return UIColor { $0.userInterfaceStyle == .dark ? dark : light }
    }
}

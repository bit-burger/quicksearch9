//
//  File.swift
//  
//
//  Created by Tony Borchert on 16.08.20.
//

import UIKit


open class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {

    open override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.preferredDisplayMode = .allVisible
        
    }
    
    public func splitViewController(
             _ splitViewController: UISplitViewController,
             collapseSecondary secondaryViewController: UIViewController,
             onto primaryViewController: UIViewController) -> Bool {
        
        // Return true to prevent UIKit from applying its default behavior
        return true
    }
}


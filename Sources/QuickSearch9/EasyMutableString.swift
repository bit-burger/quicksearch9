//
//  EasyMutableString.swift
//  TabellenTest
//
//  Created by Tony Borchert on 30.05.20.
//  Copyright © 2020 Tony Borchert. All rights reserved.
//

import UIKit

struct EasyMutableString {
    
    static let regularAttributes = [NSAttributedString.Key.backgroundColor:UIColor.clear]
    static let otherAttributes = [NSAttributedString.Key.backgroundColor:UIColor.yellow]
    
    var string:[(Character,Bool)]
    var regular:[NSAttributedString.Key:Any?]
    var other:[NSAttributedString.Key:Any?]
    
    init(string:String = String(),regular:[NSAttributedString.Key:Any] = EasyMutableString.regularAttributes, other:[NSAttributedString.Key:Any] = EasyMutableString.otherAttributes) {
        self.string = [];self.regular = regular;self.other = other
        for character in string {
            self.string.append((character,false))
        }
    }
    
    init(string:[(Character,Bool)]) {
        
        self.init()
        self.string = string
    }
    mutating func changeString(changeHandler:(Character,Int) -> (Bool)) {
        for index in 0...string.count - 1 {
            string[index].1 = changeHandler(string[index].0, index)
        }
    }
    var realString:String {
        var returnValue = String()
        for character in string {
            returnValue.append(character.0)
        }
        return returnValue
    }
    var mutableAttributedString:NSMutableAttributedString {
        get {
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(string: realString, attributes: regular as [NSAttributedString.Key : Any])
            for index in 0..<string.count {
                
                if string[index].1 {
                    attributedString.addAttributes(other as [NSAttributedString.Key : Any], range: NSRange(index...index))
                }
            }
            return attributedString
        }
    }
    
}


public extension Array where Element == (Character,Bool) {
    var inRanges:[NSRange] {
        var returnValue = [NSRange]()
        var length = 0
        var indexThen = 0
        var index = 0
        for character in self {
            if character.1 {
                length += 1
                if length == 1 {
                    indexThen = index
                }
            } else {
                if length > 0 {
                    returnValue.append(NSRange(location: indexThen, length: length))
                    length = 0
                }
            }
            index += 1
        }
        if length > 0 {
            returnValue.append(NSRange(location: indexThen, length: length))
        }
        for range in returnValue {
            print(range.location,range.length)
        }
        //print(returnValue,"inRanges")
        return returnValue
    }
    
    var inString:String {
        var returnValue = String()
        for character in self {
            returnValue.append(character.0)
        }
        return returnValue
    }
    
}
